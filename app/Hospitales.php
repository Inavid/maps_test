<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospitales extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hospitales';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = [
        'nombre',
        'latitud',
        'longitud',
      ];

}
