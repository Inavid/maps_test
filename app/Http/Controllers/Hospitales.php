<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospitales as HospitalesModel;

class Hospitales extends Controller
{
    /**
     * Index para pintar mapa
     */
    public function index() {
        return view('index');
    }

    /**
     * Esta funcion es la que obtiene los hospitales y compara la ubicacion del usuario con la de los hospitales que tengamos en la base de datos
     * 
     */
    public function getHospitals(Request $request) {
        
        #Obtenemos la ubicación del usuario que se solicito en la pagina
        $user_info = $request->input('user_info');

        #Se obtienen hospitales de la base de datos
        $hospitales = HospitalesModel::all();
        $hospitalesToDraw = [];

        foreach($hospitales as $key => $hospital) {
            $hospitalArray = $hospital->getOriginal();

            #La distancia calculada en este caso regresa en metros
            $distance = $this->vincentyGreatCircleDistance(
                $user_info['latitude'], 
                $user_info['longitude'], 
                $hospitalArray['latitud'], 
                $hospitalArray['longitud'], 
                $earthRadius = 6371000
            );

            //Solo aquellos que se encuentran a menos de 200 metros del usuario se pintaran en el mapa, 
            //como se puede ver pues ustedes definen la distancia a utilizar
            if($distance <= 2000) {
                $hospitalesToDraw[] = $hospitalArray;
            }
        }

        return response()->json([
            'success' => true,
            'data'    => $hospitalesToDraw
        ]);
    }

    /**
     * Esta función es la que hace el calculo de distancia entre el punto origen(usuario) y el punto destino(hospital)
     * @param float $latitudeFrom Latitud usuario
     * @param float $longitudeFrom Longitud usuario
     * @param float $latitudeTo Latitud hospital
     * @param float $longitudeTo Longitud hospital
     * @param float $earthRadius Este valor indica que el calculo sea en metros
     * @return float Regresa la distancia entre los puntos segun el valor de radio de la tierra (metros en este ejemplo)
     */
    public static function vincentyGreatCircleDistance(
        $latitudeFrom, 
        $longitudeFrom, 
        $latitudeTo, 
        $longitudeTo, 
        $earthRadius = 6371000)
    {
        //Convertir de grados a radianes para el calculo
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);
    
        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
        pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
    
        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }
}
