# HOW TO INSTALL

### Using docker

1.- Ejecutar el comando:  
**docker run --rm -v $(pwd):/app composer/composer install**  
2.- Ejecutar el comando:  
**docker-compose up -d**  
3.- Copiar .env.example y crear con el mismo contenido .env  
4.- Ejecutar los comandos:  
**docker-compose exec app php artisan key:generate**  
**docker-compose exec app php artisan optimize**  
**docker-compose exec app php artisan migrate --seed**  
5.- Entrar a localhost:8081/hospitales
6.- Aceptar compartir ubicación (los hospitales de prueba la mayoria se ubica en el centro de la ciudad, colonia roma)

### Without docker
1.- Hacer configuracion inicial de un proyecto de laravel:  
**https://laravel.com/docs/5.7/installation**

2.- Ejecutar migraciones  
**php artisan migrate --seed**  

3.- Entrar a la ubicacion configurada (localhost o el host que hayas configurado) y /hospitales  

4.- Aceptar compartir ubicación(los hospitales de prueba la mayoria se ubica en el centro de la ciudad, colonia roma)
