<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('images/logo_icon.ico') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'GSS') }}</title>

    <!-- Styles -->
    <!-- Latest compiled and minified CSS -->
    <link href="{{ asset('css/app.css') }}?v=15" rel="stylesheet">
    @yield('styles')

</head>
<body>
    
    @yield('content')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}?v=15"></script>
    @yield('scripts')
</body>
</html>
