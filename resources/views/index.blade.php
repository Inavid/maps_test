@extends('layouts.app')
@section('styles')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/home.css') }}?v=15" rel="stylesheet">
@endsection
@section('content')
<div id="wrapper">

    <div id="page-content-wrapper" class="active-right">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <!-- Es importante que este div tenga un width y height minimos en css de lo contrario no se pintara -->
                    <div id="map" class="embed-responsive"></div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/map.js') }}"></script>
    <!-- Es importante que dicho key se cambia por el de la cuenta de ustedes -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDG1Qrdk6PRv7CS-N88YK-99zO1i-KP-lA&callback=initMap"></script>
    <script src="{{ asset('js/home.js') }}"></script>
@endsection
