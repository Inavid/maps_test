var map;
var marker;
var polygon;
var campo_polygon;

function initMap() {
    var myLatLng = {lat: -25.363, lng: 131.044};
    var centro = {lat: 19.4142776, lng: -99.1622213};
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15, //mientras mayor sea mas cerca de la calle se posicionara al iniciar
        mapTypeId: 'roadmap', //puede ser roadmap, satellite, hybrid y terrain el que deseen utilizar
        center: centro
    });
}

function addMarker(userCoordinates){

    var coordinate = {
        lat: Number(userCoordinates.coord_latitud), 
        lng: Number(userCoordinates.coord_longitud)
    };

    marker = new google.maps.Marker({
        position: coordinate,
        //icon: icon, //Este icono puede ser la imagen que tu quieras, si no lo pones añade el default, si pones la ruta de la imagen que quieres ocupar pues utiliza dicha imagen
        map: map, //Aqui debe ir la variable del mapa que se inicializo al principio
        title: "PRUEBA" //Este titulo podria funcionar para mostrar la info del hospital al presionarlo o algo similar
    });

    marker.setMap(map); //Es importante este para asociar el marcador con el mapa

    //Todo lo siguiente te serviria para mostrar un globo con ifnormacion sobre el icono al presionarlo

    //Esta funcion prepararia el codigo html que mostraria en el cuadro de informacion
    //var contentString = prepareContent();

    //Aqui es donde definimos que se genere el globo con informacion de google maps
    // var infowindow = new google.maps.InfoWindow({
    //     content: contentString
    // });

    //Este indica que a dicho marcador se le añada el listener para que al presionarlo ejecute la funcion mencionada
    // marker.addListener('click', function() {
    //     infowindow.open(map, marker);
    // });

}

function prepareContent(data_pozo){
    var content = "";
    return content;
}