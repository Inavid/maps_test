var user;

//Esto pide la ubicacion del usuario(solicita permiso para obtenerla)
navigator.geolocation.getCurrentPosition(showPosition, errorCallback,{timeout:10000});

function showPosition(position) {
    //Obtenemos la ubicacion del usuario
    user = position;
    
    addMarker(
        {
            "coord_latitud": position.coords.latitude, 
            "coord_longitud": position.coords.longitude
        }
    );

    getClosestHospitals();
}

function errorCallback(position) {
    user = {
        coords: {
            latitude: "19.4142776",
            longitude: "-99.1622213"
        }
    };

    getClosestHospitals();
}

function getClosestHospitals() {
    var currentToken = $('meta[name="csrf-token"]').attr('content');

    //Aqui obtenemos los hospitales que se encuentren a x distancia del usuario
    $.ajax({
        type: "POST",
        url: "/getHospitals",
        data: {
            _token:currentToken,
            user_info: user.coords
        },
        async: true,
        success: function(data) {
            if(data.success) {
                $.each(data.data, function (key, val) {
                    addMarker(
                        {
                            "coord_latitud": val.latitud, 
                            "coord_longitud": val.longitud
                        }
                    );
                });
            }
        }
    });
}