<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitales', function (Blueprint $table) {
            $table->integer('id');
            $table->string('nombre');
            $table->string('ciudad');
            $table->string('direccion');
            $table->string('colonia');
            $table->string('delegacion');
            $table->string('cp');
            $table->string('horario');
            $table->string('telefono');
            $table->string('latitud');
            $table->string('longitud');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hospitales');
    }
}
