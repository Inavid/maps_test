<?php

use Illuminate\Database\Seeder;

use App\Hospitales;

class hospitalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hospital1 = new Hospitales();
        $hospital1->id = 4;
        $hospital1->nombre = "CENTRO HOSPITALARIO DE AGUASCALIENTES";
        $hospital1->ciudad = "AGUASCALIENTES";
        $hospital1->direccion = "EMILIANO ZAPATA 521";
        $hospital1->colonia = "BARRIO DE GUADALUPE";
        $hospital1->delegacion = "AGUASCALIENTES";
        $hospital1->cp = "20059";
        $hospital1->horario = "24 HRS";
        $hospital1->telefono = "9151450";
        $hospital1->latitud = "21.8825639";
        $hospital1->longitud = "-102.3065306";
        $hospital1->save();

        $hospital2 = new Hospitales();
        $hospital2->id = 8;
        $hospital2->nombre = "HOSPITAL CARDIOLOGICA AGUASCALIENTES";
        $hospital2->ciudad = "AGUASCALIENTES";
        $hospital2->direccion = "ECUADOR 200";
        $hospital2->colonia = "LAS AMERICAS";
        $hospital2->delegacion = "AGUASCALIENTES";
        $hospital2->cp = "20230";
        $hospital2->horario = "24 HRS";
        $hospital2->telefono = "9154000";
        $hospital2->latitud = "21.8701222";
        $hospital2->longitud = "-102.296466666666";
        $hospital2->save();

        $hospital3 = new Hospitales();
        $hospital3->id = 10;
        $hospital3->nombre = "STAR MEDICA DE AGUASCALIENTES";
        $hospital3->ciudad = "AGUASCALIENTES";
        $hospital3->direccion = "AV UNIVERSIDAD 101";
        $hospital3->colonia = "VILLAS DE LA UNIVERSIDAD";
        $hospital3->delegacion = "AGUASCALIENTES";
        $hospital3->cp = "20020";
        $hospital3->horario = "24 HRS";
        $hospital3->telefono = "9109900";
        $hospital3->latitud = "21.8975306";
        $hospital3->longitud = "-102.304563888888";
        $hospital3->save();

        $hospital4 = new Hospitales();
        $hospital4->id = 12;
        $hospital4->nombre = "HOSPITAL SAN JOSE";
        $hospital4->ciudad = "ENSENADA";
        $hospital4->direccion = "AV LAZARO CARDENAS 173";
        $hospital4->colonia = "CHAPULTEPEC";
        $hospital4->delegacion = "ENSENADA";
        $hospital4->cp = "22785";
        $hospital4->horario = "24 HRS";
        $hospital4->telefono = "1774757";
        $hospital4->latitud = "31.8113583";
        $hospital4->longitud = "-116.594477777777";
        $hospital4->save();

        $hospital5 = new Hospitales();
        $hospital5->id = 13;
        $hospital5->nombre = "HOSPITAL SANTA ROSA DE LIMA";
        $hospital5->ciudad = "ENSENADA";
        $hospital5->direccion = "ITURBIDE 399";
        $hospital5->colonia = "ENSENADA CENTRO";
        $hospital5->delegacion = "ENSENADA";
        $hospital5->cp = "22800";
        $hospital5->horario = "24 HRS";
        $hospital5->telefono = "1724757";
        $hospital5->latitud = "31.8610556";
        $hospital5->longitud = "-116.611816666666";
        $hospital5->save();

        $hospital6 = new Hospitales();
        $hospital6->id = 18;
        $hospital6->nombre = "HOSPITAL ALMATER";
        $hospital6->ciudad = "MEXICALI";
        $hospital6->direccion = "AV MADERO 1060";
        $hospital6->colonia = "NUEVA";
        $hospital6->delegacion = "MEXICALI";
        $hospital6->cp = "21100";
        $hospital6->horario = "24 HRS";
        $hospital6->telefono = "5534015";
        $hospital6->latitud = "32.6667111";
        $hospital6->longitud = "-115.45984166666668";
        $hospital6->save();

        $hospital7 = new Hospitales();
        $hospital7->id = 19;
        $hospital7->nombre = "HOSPITAL ARCE QUIÑONES";
        $hospital7->ciudad = "MEXICALI";
        $hospital7->direccion = "CALLEJON RIO TAMAZULA SUR 2600";
        $hospital7->colonia = "GONZALEZ ORTEGA";
        $hospital7->delegacion = "MEXICALI";
        $hospital7->cp = "21397";
        $hospital7->horario = "24 HRS";
        $hospital7->telefono = "5612484";
        $hospital7->latitud = "32.5927472";
        $hospital7->longitud = "-115.3961";
        $hospital7->save();

        $hospital8 = new Hospitales();
        $hospital8->id = 177;
        $hospital8->nombre = "CENTRO MEDICO DALINDE";
        $hospital8->ciudad = "CIUDAD DE MEXICO";
        $hospital8->direccion = "TUXPAN 25";
        $hospital8->colonia = "ROMA SUR";
        $hospital8->delegacion = "CUAUHTEMOC";
        $hospital8->cp = "06760";
        $hospital8->horario = "24 HRS";
        $hospital8->telefono = "52652800";
        $hospital8->latitud = "19.4057528";
        $hospital8->longitud = "-99.16786666666667";
        $hospital8->save();

        $hospital9 = new Hospitales();
        $hospital9->id = 178;
        $hospital9->nombre = "CENTRO MEDICO TIBER";
        $hospital9->ciudad = "CIUDAD DE MEXICO";
        $hospital9->direccion = "RIO TIBER 21";
        $hospital9->colonia = "CUAUHTEMOC";
        $hospital9->delegacion = "CUAUHTEMOC";
        $hospital9->cp = "6500";
        $hospital9->horario = "24 HRS";
        $hospital9->telefono = "52083830";
        $hospital9->latitud = "19.4325667";
        $hospital9->longitud = "-99.17074722222223";
        $hospital9->save();

        $hospital10 = new Hospitales();
        $hospital10->id = 180;
        $hospital10->nombre = "HOSPITAL ANGELES CLINICA LONDRES";
        $hospital10->ciudad = "CIUDAD DE MEXICO";
        $hospital10->direccion = "DURANGO 50";
        $hospital10->colonia = "ROMA";
        $hospital10->delegacion = "CUAUHTEMOC";
        $hospital10->cp = "06700";
        $hospital10->horario = "24 HRS";
        $hospital10->telefono = "52298400";
        $hospital10->latitud = "19.4217333";
        $hospital10->longitud = "-99.15711111111112";
        $hospital10->save();

        $hospital11 = new Hospitales();
        $hospital11->id = 181;
        $hospital11->nombre = "HOSPITAL ANGELES METROPOLITANO";
        $hospital11->ciudad = "CIUDAD DE MEXICO";
        $hospital11->direccion = "TLACOTALPAN 51 Y 59";
        $hospital11->colonia = "ROMA SUR";
        $hospital11->delegacion = "CUAUHTEMOC";
        $hospital11->cp = "06700";
        $hospital11->horario = "24 HRS";
        $hospital11->telefono = "52651800";
        $hospital11->latitud = "19.4068556";
        $hospital11->longitud = "-99.16690833333334";
        $hospital11->save();

        $hospital12 = new Hospitales();
        $hospital12->id = 182;
        $hospital12->nombre = "HOSPITAL ANGELES ROMA (SANTELENA)";
        $hospital12->ciudad = "CIUDAD DE MEXICO";
        $hospital12->direccion = "QUERETARO 58";
        $hospital12->colonia = "ROMA";
        $hospital12->delegacion = "CUAUHTEMOC";
        $hospital12->cp = "06700";
        $hospital12->horario = "24 HRS";
        $hospital12->telefono = "55747000";
        $hospital12->latitud = "19.4151889";
        $hospital12->longitud = "-99.15847222222223";
        $hospital12->save();
                
        $hospital13 = new Hospitales();
        $hospital13->id = 183;
        $hospital13->nombre = "HOSPITAL MARIAJOSE";
        $hospital13->ciudad = "CIUDAD DE MEXICO";
        $hospital13->direccion = "COZUMEL 62";
        $hospital13->colonia = "ROMA";
        $hospital13->delegacion = "CUAUHTEMOC";
        $hospital13->cp = "06700";
        $hospital13->horario = "24 HRS";
        $hospital13->telefono = "55140568";
        $hospital13->latitud = "19.4180083";
        $hospital13->longitud = "-99.17059166666667";
        $hospital13->save();
        
        $hospital14 = new Hospitales();
        $hospital14->id = 184;
        $hospital14->nombre = "HOSPITAL OBREGON";
        $hospital14->ciudad = "CIUDAD DE MEXICO";
        $hospital14->direccion = "ALVARO OBREGON 123";
        $hospital14->colonia = "ROMA";
        $hospital14->delegacion = "CUAUHTEMOC";
        $hospital14->cp = "06700";
        $hospital14->horario = "24 HRS";
        $hospital14->telefono = "55255000";
        $hospital14->latitud = "19.4182167";
        $hospital14->longitud = "-99.16082777777778";
        $hospital14->save();

        $hospital15 = new Hospitales();
        $hospital15->id = 185;
        $hospital15->nombre = "HOSPITAL SAN ANGEL INN CHAPULTEPEC";
        $hospital15->ciudad = "CIUDAD DE MEXICO";
        $hospital15->direccion = "AV CHAPULTEPEC 489";
        $hospital15->colonia = "JUAREZ";
        $hospital15->delegacion = "CUAUHTEMOC";
        $hospital15->cp = "6600";
        $hospital15->horario = "24 HRS";
        $hospital15->telefono = "52411700";
        $hospital15->latitud = "19.4216833";
        $hospital15->longitud = "-99.17294166666667";
        $hospital15->save();
        
        $hospital16 = new Hospitales();
        $hospital16->id = 186;
        $hospital16->nombre = "HOSPITAL STAR MEDICA CENTRO (SANTA FE)";
        $hospital16->ciudad = "CIUDAD DE MEXICO";
        $hospital16->direccion = "SAN LUIS POTOSI 143";
        $hospital16->colonia = "ROMA";
        $hospital16->delegacion = "CUAUHTEMOC";
        $hospital16->cp = "06060";
        $hospital16->horario = "24 HRS";
        $hospital16->telefono = "10844733";
        $hospital16->latitud = "19.4138528";
        $hospital16->longitud = "-99.16123333333334";
        $hospital16->save();
        
        $hospital17 = new Hospitales();
        $hospital17->id = 187;
        $hospital17->nombre = "HOSPITAL TRINIDAD";
        $hospital17->ciudad = "CIUDAD DE MEXICO";
        $hospital17->direccion = "MANZANILLO 94";
        $hospital17->colonia = "ROMA SUR";
        $hospital17->delegacion = "CUAUHTEMOC";
        $hospital17->cp = "06760";
        $hospital17->horario = "24 HRS";
        $hospital17->telefono = "52641081";
        $hospital17->latitud = "19.4075417";
        $hospital17->longitud = "-99.1649388888889";
        $hospital17->save();
        
        $hospital18 = new Hospitales();
        $hospital18->id = 189;
        $hospital18->nombre = "INSTITUTO NACIONAL DE CIRUGIA CARDIACA Y CARDIOLOG";
        $hospital18->ciudad = "CIUDAD DE MEXICO";
        $hospital18->direccion = "QUERETARO 58";
        $hospital18->colonia = "ROMA";
        $hospital18->delegacion = "CUAUHTEMOC";
        $hospital18->cp = "06700";
        $hospital18->horario = "24 HRS";
        $hospital18->telefono = "55750310";
        $hospital18->latitud = "19.3938944";
        $hospital18->longitud = "-99.16233333333334";
        $hospital18->save();
        
        $hospital19 = new Hospitales();
        $hospital19->id = 190;
        $hospital19->nombre = "MEDICA SAN LUIS";
        $hospital19->ciudad = "CIUDAD DE MEXICO";
        $hospital19->direccion = "SAN LUIS POTOSI 122";
        $hospital19->colonia = "ROMA";
        $hospital19->delegacion = "CUAUHTEMOC";
        $hospital19->cp = "06700";
        $hospital19->horario = "24 HRS";
        $hospital19->telefono = "10546790";
        $hospital19->latitud = "19.4139444";
        $hospital19->longitud = "-99.16122222222222";
        $hospital19->save();
        
        $hospital20 = new Hospitales();
        $hospital20->id = 191;
        $hospital20->nombre = "NUEVO SANATORIO DURANGO";
        $hospital20->ciudad = "CIUDAD DE MEXICO";
        $hospital20->direccion = "DURANGO 296";
        $hospital20->colonia = "ROMA";
        $hospital20->delegacion = "CUAUHTEMOC";
        $hospital20->cp = "06700";
        $hospital20->horario = "24 HRS";
        $hospital20->telefono = "51484607";
        $hospital20->latitud = "19.4183694";
        $hospital20->longitud = "-99.17258611111112";
        $hospital20->save();
        
        $hospital21 = new Hospitales();
        $hospital21->id = 197;
        $hospital21->nombre = "HOSPITAL DE COS";
        $hospital21->ciudad = "CIUDAD DE MEXICO";
        $hospital21->direccion = "GUANAJUATO 178";
        $hospital21->colonia = "ROMA NORTE";
        $hospital21->delegacion = "CUAUHTEMOC";
        $hospital21->cp = "06700";
        $hospital21->horario = "24 HRS";
        $hospital21->telefono = "52083694";
        $hospital21->latitud = "19.4161306";
        $hospital21->longitud = "-99.16248888888889";
        $hospital21->save();
        
    }
}
